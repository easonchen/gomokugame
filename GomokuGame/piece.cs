﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace GomokuGame
{
    abstract class Piece: PictureBox // "abstract" is because the "Piece" class itself doesn't have meaning.
    {
        private static readonly int IMAGE_WIDTH = 50; // the size of the piece

        public Piece(int x, int y)
        {
            this.BackColor = Color.Transparent;

            // let the location be the cneter of the piece.
            this.Location = new Point(x - IMAGE_WIDTH / 2, y - IMAGE_WIDTH / 2); 

            this.Size = new Size(IMAGE_WIDTH, IMAGE_WIDTH);
        }

        // Polymorphism, let the derived class decide its type.
        public abstract PieceType GetPieceType(); 

    }
}
