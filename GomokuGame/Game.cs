﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GomokuGame
{
    class Game
    {
        private Board board = new Board();

        public static PieceType currentPlayer = PieceType.BLACK; // the first player is black by default.

        private PieceType winner = PieceType.NONE;
        public PieceType Winner { get { return winner; } }

        public bool CanBePlaced(int x, int y)
        {
            return board.CanBePlaced(x, y);
        }

        public Piece PlaceAPiece(int x, int y)
        {
            Piece piece = board.PlaceAPiece(x, y, currentPlayer);
            if (piece != null) 
            {
                checkWinner(); // To check the winner first

                // swich the player
                if (currentPlayer == PieceType.BLACK)
                {
                    currentPlayer = PieceType.WHITE;
                }
                else if (currentPlayer == PieceType.WHITE)
                {
                    currentPlayer = PieceType.BLACK;
                }               

                return piece;
            }
            return null;
        }

        private void checkWinner()
        {
            int centerX = board.LastPlacedNode.X;
            int centerY = board.LastPlacedNode.Y;
            int numbers_takes_to_win = 6;

            // recording the same color piece in 8 direction.
            int[,] count = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
            for (int xDir = -1; xDir <= 1; xDir++)
            {
                for (int yDir = -1; yDir <= 1; yDir++)
                {
                    if (xDir == 0 && yDir == 0) // erase the middle case
                        continue;
                     
                    while (count[xDir+1, yDir+1] < numbers_takes_to_win)
                    {
                        int targetX = centerX + count[xDir + 1, yDir + 1] * xDir;
                        int targetY = centerY + count[xDir + 1, yDir + 1] * yDir;

                        // check the same color
                        if (targetX < 0 || targetX >= Board.NODE_COUNT ||
                            targetY < 0 || targetY >= Board.NODE_COUNT ||
                            board.GetPieceType(targetX, targetY) != currentPlayer)
                            break;

                        count[xDir + 1, yDir + 1]++;
                    }                    
                }
            }

            // To check whether 5 same color pieces are meet in 8 direction.
            if (count[0, 0] + count[2, 2] == (numbers_takes_to_win+1) ||
                count[1, 0] + count[1, 2] == (numbers_takes_to_win + 1) ||
                count[2, 0] + count[0, 2] == (numbers_takes_to_win + 1) ||
                count[0, 1] + count[2, 1] == (numbers_takes_to_win + 1))
            {
                winner = currentPlayer;
            }                

        }

    }
}
