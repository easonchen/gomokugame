﻿namespace GomokuGame
{
    partial class Form_Gomoku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_reminder = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_reminder)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_reminder
            // 
            this.pictureBox_reminder.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_reminder.InitialImage = null;
            this.pictureBox_reminder.Location = new System.Drawing.Point(12, 12);
            this.pictureBox_reminder.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox_reminder.Name = "pictureBox_reminder";
            this.pictureBox_reminder.Size = new System.Drawing.Size(50, 50);
            this.pictureBox_reminder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_reminder.TabIndex = 0;
            this.pictureBox_reminder.TabStop = false;
            // 
            // Form_Gomoku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GomokuGame.Properties.Resources.board;
            this.ClientSize = new System.Drawing.Size(732, 703);
            this.Controls.Add(this.pictureBox_reminder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form_Gomoku";
            this.Text = "Gomoku";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form_Gomoku_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_reminder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_reminder;
    }
}

