﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GomokuGame
{
    public partial class Form_Gomoku : Form
    {
        private Game game = new Game(); // In charge of the gaming rule.

        public Form_Gomoku(PieceType type)
        {
            InitializeComponent();
            Game.currentPlayer = type;
            // Just to remind the user the current player
            if (Game.currentPlayer== PieceType.BLACK)
                pictureBox_reminder.Image = Properties.Resources.black;
            else if (Game.currentPlayer == PieceType.WHITE)
                pictureBox_reminder.Image = Properties.Resources.white;
            else
                pictureBox_reminder.Image = Properties.Resources.black;

            // set the form's size itself.
            this.Size = new Size(750, 750);
        }

        private void Form_Gomoku_MouseMove(object sender, MouseEventArgs e)
        {
            if (game.CanBePlaced(e.X, e.Y)) // "e.X" and "e.Y" are the location of the pointer of the mouse.
                Cursor = Cursors.Hand; // change the image of the pointer of the mouse to a hand-shape image.
            else
                Cursor = Cursors.Default;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            Piece piece = game.PlaceAPiece(e.X, e.Y);
            if (piece != null)
            {
                this.Controls.Add(piece);

                // check who wins
                if (game.Winner == PieceType.BLACK)
                {
                    Form2 frm2 = new Form2(game.Winner);
                    frm2.Show();
                }
                else if (game.Winner == PieceType.WHITE)
                {
                    Form2 frm2 = new Form2(game.Winner);
                    frm2.Show();
                }

                // Just to remind the user the current player
                if (Game.currentPlayer == PieceType.BLACK)
                    pictureBox_reminder.Image = Properties.Resources.black;
                else if (Game.currentPlayer == PieceType.WHITE)
                    pictureBox_reminder.Image = Properties.Resources.white;
                else
                    pictureBox_reminder.Image = Properties.Resources.black;
            }
        }
        
    }
}
