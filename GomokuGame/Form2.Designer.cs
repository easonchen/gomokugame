﻿namespace GomokuGame
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_quit = new System.Windows.Forms.Button();
            this.button_again = new System.Windows.Forms.Button();
            this.label_promt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_quit
            // 
            this.button_quit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_quit.Location = new System.Drawing.Point(29, 54);
            this.button_quit.Name = "button_quit";
            this.button_quit.Size = new System.Drawing.Size(98, 40);
            this.button_quit.TabIndex = 0;
            this.button_quit.Text = "Quit";
            this.button_quit.UseVisualStyleBackColor = true;
            this.button_quit.Click += new System.EventHandler(this.button_quit_Click);
            // 
            // button_again
            // 
            this.button_again.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_again.Location = new System.Drawing.Point(133, 54);
            this.button_again.Name = "button_again";
            this.button_again.Size = new System.Drawing.Size(98, 40);
            this.button_again.TabIndex = 1;
            this.button_again.Text = "Again";
            this.button_again.UseVisualStyleBackColor = true;
            this.button_again.Click += new System.EventHandler(this.button_again_Click);
            // 
            // label_promt
            // 
            this.label_promt.AutoSize = true;
            this.label_promt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_promt.Location = new System.Drawing.Point(38, 27);
            this.label_promt.Name = "label_promt";
            this.label_promt.Size = new System.Drawing.Size(182, 24);
            this.label_promt.TabIndex = 2;
            this.label_promt.Text = "The winner is: Black!";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 128);
            this.Controls.Add(this.label_promt);
            this.Controls.Add(this.button_again);
            this.Controls.Add(this.button_quit);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_quit;
        private System.Windows.Forms.Button button_again;
        private System.Windows.Forms.Label label_promt;
    }
}