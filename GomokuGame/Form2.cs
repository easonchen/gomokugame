﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace GomokuGame
{
    public partial class Form2 : Form
    {
        public Form2(PieceType pieceType)
        {
            InitializeComponent();

            // check who wins
            if (pieceType == PieceType.BLACK)
            {
                label_promt.Text = "The winner is: Black!";
            }
            else if (pieceType == PieceType.WHITE)
            {
                label_promt.Text = "The winner is: White!";
            }
        }

        private void button_quit_Click(object sender, EventArgs e)
        {
            this.Close();
            Form_Gomoku.ActiveForm.Close();
            
        }

        private void button_again_Click(object sender, EventArgs e)
        {
            this.Close();
            Form_Gomoku.ActiveForm.Close();
            Thread oThreadA = new Thread(new ThreadStart(justdoit));
            oThreadA.Start();
        }

        public void justdoit()
        {
            Application.Run(new Form_Gomoku(PieceType.BLACK));
        }
    }
}
