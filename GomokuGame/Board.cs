﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GomokuGame
{
    class Board
    {
        public static readonly int NODE_COUNT = 9;

        private static readonly Point NO_MATCH_NODE = new Point(-1, -1);

        private static readonly int OFFSET = 75; // the width of the boundary
        private static readonly int NODE_RADIUS = 15; // a range to check a placeable place.
        private static readonly int NODE_DISTANCE = 75; // the distance bettween placeable places.

        private Piece[,] pieces = new Piece[NODE_COUNT, NODE_COUNT]; // two-dimentional piece number(0~8, 0~8)

        private Point lastPlaceNode = NO_MATCH_NODE;
        public Point LastPlacedNode { get { return lastPlaceNode ; } } // read-only for outside

        public bool CanBePlaced(int x, int y)
        {
            Point nodeID = findTheClosetNode(x, y); // "Point" is a datatype from "System.Drawing".

            if (nodeID == NO_MATCH_NODE)
                return false;

            if (pieces[nodeID.X, nodeID.Y] != null)
                return false;

            return true;
        }

        // "private" is used for the class itself. The first letter of the name should be lower case.
        private Point findTheClosetNode(int x, int y) // return the 2-dimensional "Point".
        {
            int nodeIDX = findTheClosetNode(x);
            if (nodeIDX == -1)
                return NO_MATCH_NODE;

            int nodeIDY = findTheClosetNode(y);
            if (nodeIDY == -1)
                return NO_MATCH_NODE;

            return new Point(nodeIDX, nodeIDY);
        }

        private int findTheClosetNode(int pos) // check the legality of the one-dimensional direction.
        {
            if (pos < OFFSET - NODE_RADIUS) // check the piece is in the placeable place.
                return -1;

            pos -= OFFSET; // the shifted coordinate for placeable area.

            int quotient = pos / NODE_DISTANCE; // the number of the placeable place.
            int remainder = pos % NODE_DISTANCE; // being used to determine which number it belones.

            // the number of pieces are with in 0~8 bacause it's a 9X9 board.
            if (quotient< NODE_COUNT && remainder <= NODE_RADIUS) 
                return quotient;
            else if (quotient+1 < NODE_COUNT && remainder >= NODE_DISTANCE - NODE_RADIUS)
                return quotient + 1;
            else
                return -1;
        }

        public Piece PlaceAPiece(int x, int y, PieceType type)
        {
            Point nodeID = findTheClosetNode(x, y);

            if (nodeID == NO_MATCH_NODE)
                return null;

            if (pieces[nodeID.X, nodeID.Y] != null) // the place has been placed a piece.
                return null;

            Point formPos = convertToFormPosition(nodeID);
            if (type == PieceType.BLACK)
                pieces[nodeID.X, nodeID.Y] = new BlackPiece(formPos.X, formPos.Y);
            else if (type == PieceType.WHITE)
                pieces[nodeID.X, nodeID.Y] = new WhitePiece(formPos.X, formPos.Y);

            lastPlaceNode = nodeID; // recording the last node the piece placed

            return pieces[nodeID.X, nodeID.Y];
        }

        private Point convertToFormPosition(Point nodeID) // let the pieces neat by give a fix form position.
        {
            Point formPosition = new Point();
            formPosition.X = nodeID.X * NODE_DISTANCE + OFFSET;
            formPosition.Y = nodeID.Y * NODE_DISTANCE + OFFSET;
            return formPosition;
        }

        // to know the color of the piece of the place.
        public PieceType GetPieceType(int nodeIDX, int nodeIDY)
        {
            if (pieces[nodeIDX, nodeIDY] == null)
                return PieceType.NONE;
            else
                return pieces[nodeIDX, nodeIDY].GetPieceType();
        }

    }
}
